# coding: utf-8
#
# STEN, SAX OCH PÅSE
# ==================

import random
import sys
import time

moves = ('rock', 'paper', 'scissors')
ties = 0
cpu_wins = 0
pl_wins = 0

def get_bad_word():
    bad_word_list = ['\033[1;34mSuck my balls!\033[1;m',
                     '\033[1;34mAll aboard your face!\033[1;m',
                     '\033[1;34mYo mama so ugly she has a face!\033[1;m',
                     "\033[1;34mYou don't have the balls to beat me!\033[1;m",
                     '\033[1;34mFuck you bitch, LOL!\033[1;m',
                     '\033[1;34mCan you spell defeat?\033[1;m',
                     '\033[1;34mYou have a beer belly LOL!!!\033[1;m']
    return random.sample(bad_word_list, 1)[0]

def get_player_name():
    name = input("What's your name? ")
    if name == 'Fredde' or name == 'fredde':
        print('Do you know what you have?')
        print('A beer belly!!! LOL!!!')
    return name

def how_many_matches():
    while True:
        try:
            matches = int(input('How many matches do you want to play? '))
            return(matches)
        except:
            print("That's not a number")

def enable_cheats(matches):
    cheat_on = input('Do you want the computer to cheat? ').lower().strip()
    if cheat_on == 'yes':
        return True
    elif cheat_on == 'no':
        return False
    else:
        print('You can only say Yes or No!')
        return enable_cheats(matches)
        
def show_score():
    scoreboard = input('Do you want to see the score after every match? ').lower().strip()
    if scoreboard == 'yes':
        return True
    elif scoreboard == 'no':
        return False
    else:
        print('You can only say Yes or No!')
        return show_score()

def computer_move(player, cheat_on):
    global ties, cpu_wins, pl_wins
    move = moves[random.randint(0,2)]
    if cheat_on:
        will_cheat = random.randint(0,5)
        if will_cheat == 0:
            print('\033[1;34mI think I deserve another point!\033[1;m')
            print("\033[1;34mThe computer's score went up by one!\033[1;m")
            cpu_wins += 1
        elif will_cheat == 1:
            print(get_bad_word())
            if player == 'rock':
                move = 'paper'
            elif player == 'paper':
                move = 'scissors'
            elif player == 'scissors':
                move = 'rock'
        elif will_cheat == 2:
            if pl_wins > cpu_wins:
                print("\033[1;34mLet's call it a day!\033[1;m")
                sys.exit()
            else:
                print(get_bad_word())
        elif will_cheat == 3:
            if pl_wins > 0:
                print('\033[1;34mLook! An air balloon!\033[1;m')
                time.sleep(1)
                print('\033[1;34mWhile you looked at the balloon the computer deleted all of your wins.\033[1;m')
                pl_wins = 0
            else:
                print(get_bad_word())
        elif will_cheat == 4 or will_cheat == 5:
            print(get_bad_word())
    return move
    
def player_move():
    move = input('Rock, Paper or Scissors? ').lower().strip()
    if move in moves:
        return move
    else:
        print('Invalid move')
        return player_move()
        
def play_matches(matches, name, cheat_on, scoreboard):
     loop = 0
     while loop < matches:
            loop += 1
            pl_move = player_move()
            cpu_move = computer_move(pl_move, cheat_on)
            print(result(pl_move, cpu_move, name))
            if scoreboard and loop < matches:
                print('\033[1;31mAmount of times ' + name + ' has won: %d\033[1;m' % pl_wins)
                print('\033[1;31mAmount of times the computer has won: %d\033[1;m' % cpu_wins)
                print('\033[1;31mAmount of ties: %d\033[1;m' % ties)
    
def result(player, cpu, name):
    global ties, cpu_wins, pl_wins
    #---------------------------------
    #If it's a tie do this
    #---------------------------------
    if player == cpu:
        ties += 1
        return 'Did you chose ' + cpu + " too " + name + "?! That means it's a tie and you won't die in a pie."
    #---------------------------------
    #If the player wins do this
    #---------------------------------
    if player == "rock" and cpu == "scissors":
        answer = "Damn it, " + name + " you killed my " + cpu + " with your stupid " + player + '!'
        pl_wins += 1
    elif player == "scissors" and cpu == "paper":
        answer = "Damn it, " + name + " you killed my " + cpu + " with your stupid " + player + '!'
        pl_wins += 1
    elif player == "paper" and cpu == "rock":
        answer = "Damn it, " + name + " you killed my " + cpu + " with your stupid " + player + '!'
        pl_wins += 1
    #---------------------------------
    #This only happens if the computer wins
    #---------------------------------
    else:
        answer = "Ha ha, I beat you " + name + " and your " + player + " with my " + cpu + "!"
        cpu_wins += 1
    return answer
 

def print_result(name):
    if pl_wins == 1:
        print('\033[1;32mYou got 1 win, {}!\033[1;m'.format(name))
    else:
        print('\033[1;32mYou got {} wins, {}!\033[1;m'.format(pl_wins,name))
    if cpu_wins == 1:
        print('\033[1;32mComputer got 1 win!\033[1;m')
    else:
        print('\033[1;32mComputer got {} wins!\033[1;m'.format(cpu_wins))
    if ties == 1:
        print('\033[1;32mThere was 1 tie!\033[1;m')
    else:
        print('\033[1;32mThere were {} ties!\033[1;m'.format(ties))
    
def main():
        name = get_player_name()
        matches = how_many_matches()
        cheat_on = enable_cheats(matches)
        if cheat_on and matches < 10:
            matches = 10
            print("\033[1;34mYou have to play at least 10 matches, don't think you can get away " + name + '!\033[1;m')
        scoreboard = show_score()
        play_matches(matches, name, cheat_on, scoreboard)
        print_result(name)
main()
